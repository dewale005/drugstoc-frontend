import { createContext, useState, useEffect } from "react";
import { SEARCH_PRODUCT } from "../constants/_api.constant";
const GlobalContext: any = createContext(1);

const GlobalProviderContext = ({ children }: any) => {
  const [dataGlobalContext, setData] = useState<any>();
  const [query, setQuery] = useState("");

  useEffect(() => {
    fetchAPI();
  }, [query]);

  const filter = (e: any) => {
    setQuery(e.target?.value);
  };

  const fetchAPI = async () => {
    const res = await fetch(
      `https://staging.drugstoc.com${SEARCH_PRODUCT}${query}`
    );
    const data = await res.json();
    setData(data);
  };
  return (
    <GlobalContext
      value={{
        dataGlobalContext,
      }}
    >
      {children}
    </GlobalContext>
  );
};

export default GlobalProviderContext;
