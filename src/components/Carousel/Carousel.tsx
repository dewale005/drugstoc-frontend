import { useCallback, useEffect, /*useRef,*/ useState } from "react";
import { MdArrowBackIos } from "react-icons/md";
import { MdArrowForwardIos } from "react-icons/md";
import "./Carousel.css";
import { Box } from "@mui/material";

const photos = [
  {
    id: "p1",
    title: "Personal Protective Overall Banner",
    url: "https://res.cloudinary.com/bizstak/image/upload/v1661622298/image3_lda1ei.png",
  },
  {
    id: "p2",
    title: "Business Expand with Drugstoc",
    url: "https://res.cloudinary.com/bizstak/image/upload/v1661622308/image2_emlri1.png",
  },
];

function App() {
  const [currentIndex, setCurrentIndex] = useState(0);

  const next = useCallback(() => {
    setCurrentIndex((currentIndex + 1) % photos.length);
  }, [currentIndex]);

  const prev = () => {
    setCurrentIndex((currentIndex - 1 + photos.length) % photos.length);
  };

  useEffect(() => {
    const handleAutoplay = setInterval(next, 5000);
    return () => clearInterval(handleAutoplay);
  }, [next]);

  return (
    <>
      <Box className="slider-container">
        {photos.map((photo) => (
          <div
            key={photo.id}
            className={
              photos[currentIndex].id === photo.id ? "fade" : "slide fade"
            }
          >
            <img src={photo.url} alt={photo.title} className="photo" />
          </div>
        ))}

        <button onClick={prev} className="prev">
          <MdArrowBackIos size={25} />
        </button>

        <button onClick={next} className="next">
          <MdArrowForwardIos size={25} />
        </button>
      </Box>

      <div className="dots">
        {photos.map((photo) => (
          <span
            key={photo.id}
            className={
              photos[currentIndex].id === photo.id ? "dot active" : "dot"
            }
            onClick={() => setCurrentIndex(photos.indexOf(photo))}
          ></span>
        ))}
      </div>
    </>
  );
}

export default App;
