import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export default function Loading() {
  return (
    <Box sx={{textAlign: 'center', my: 1, mx: 'auto', width: '100%'}}>
      <CircularProgress />
    </Box>
  );
}